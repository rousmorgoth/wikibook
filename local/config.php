<?php if (!defined('PmWiki')) exit();

#### Zakladni nastaveni webu ####
$DefaultName = 'Home'; # Nazev vychozi strany
$DefaultGroup = 'WikiBook'; # Nazev vychozi skupiny
$SiteAdminGroup = $SiteGroup; # Nazev administratorske skupiny
$WikiTitle = 'WikiBook'; # Titulek webu
$PageLogoUrl = 'http://adresawebu/logo.png'; # URL hlavn�ho loga *****
$ScriptUrl = 'http://adresawebu'; # URL hlavniho skriptu *****
$PubDirUrl = 'http://adresawebu/pub'; # URL slozky se skiny *****

#### Nastaveni narodni podpory ####
XLPage('cs','PmWikiCs.XLPage');
XLPage('cs','PmWikiCS.XLPageCookbook');

#### Nastaveni k�dov�n� UTF-8 ####
include_once("scripts/xlpage-utf-8.php");
$DefaultPageCharset = array(''=>'ISO-8859-2', 'ISO-8859-1'=>'ISO-8859-2');

#### Dalsi nastaveni webu ####
$DiffKeepDays = 0; # vymazani historie uprav starsich jak 3 mesice
$EnablePathInfo = 1;# Zobrazeni URL v adresnim radku
$EnableLinkPageRelative = 1; # Podpora relativnich linku
$EnablePostAuthorRequired = 0; # Vy�adov�n autor clanku

### Nastaveni vzhledu ####
$Skin = 'wikibook'; # Vychozi skin

## Vlastnosti vzhledu
$DefaultColor = 'dark'; # Vychozi barevne schema (light / dark)
$DefaultFont = 'arial'; # Vychozi typ pisma

#### Nastaven� hesla pro pristup ####
$DefaultPasswords['admin'] = crypt('admin'); # heslo pro upravy menu a ovladani
$DefaultPasswords['edit'] = crypt('edit'); # heslo pro upravu stranek
$DefaultPasswords['upload'] = crypt('upload'); # heslo pro upload souboru

#### Nastaveni uploadu soubou ####
$EnableUpload = 1; # povoleni moznosti uploadu
$UploadMaxSize = 1000000; # limit pro velikost soubor� 1MB 
$UploadExtSize['zip'] = 2000000; # limit pro velikost uploadu zip archiv� 2 MB
$UploadExtSize['rar'] = 2000000; # limit pro velikost uploadu rar archiv� 2 MB
$UploadExts['swf'] = 'flash/swf'; # swf pro upload
$UploadExts['exe'] = 'execute/exe'; # exe pro upload
$UploadExts['dwf'] = 'autocad/dwf'; # dwf pro upload
$EnableUploadOverwrite = 1; # zapnuti moznosti prepisu puvodniho souboru

#### Nacteni skriptu pro podporu dalsich funkci ####
include_once("$FarmD/cookbook/gallery.php"); # jednoducha fotogalerie
include_once("$FarmD/cookbook/enablehtml.php"); # povoleni html tagu uvedenych dale
include_once("$FarmD/cookbook/thumblink.php"); # automaticke nahledy
include_once("$FarmD/cookbook/toggle.php"); # Tla��tka a odkazy
include_once("$FarmD/cookbook/syntaxlove.php"); # Zv�razn�n� syntaxe
include_once("$FarmD/cookbook/pmwiki2pdf-v2/pmwiki2pdf.php"); # HTML2PDF
include_once("$FarmD/cookbook/includeurl.php"); # include URL
include_once("$FarmD/cookbook/swf-sites.php"); # YouTube, Vimeo, Google Video
include_once("$FarmD/cookbook/newpageboxplus.php"); # Vytv��en� nov�ch str�nek

##--------------------------------------------------------------------------##

### Povoleni pouziti HTML znacek
EnableHtml('img');
EnableHtml('a|b|br|p|hr|h1|h2|h3|blockquote|u|ol|li|i|u|sup|sub|div|style|center|iframe|embed|script|!--');

#### Nastaveni prohlizece
$EnableDiag = 1; # zapnuti diagnostiky
$EnableIMSCaching = 0; # vyuziti vyrovnavaca pameti prohlizece

#### Nastaveni podpory WikiSlov ####
$SpaceWikiWords = 0; # zapnuti Wiki Slov s podporou mezer
$LinkWikiWords = 0; # zapnuti tvorby linku z WikiSlov - WikiWords

#### Podpora RSS kanalu ####
# if ($action == 'rss') include_once('scripts/feeds.php');   # RSS 2.0
# if ($action == 'atom') include_once('scripts/feeds.php');  # Atom 1.0
# if ($action == 'dc') include_once('scripts/feeds.php');    # Dublin Core
# if ($action == 'rdf') include_once('scripts/feeds.php');   # RSS 1.0

#### Definice specialnich znacek ####
Markup("'~", "inline", "/'~(.*?)~'/", "<i>$1</i>");        # '~italic~'
Markup("'*", "inline", "/'\\*(.*?)\\*'/", "<b>$1</b>");    # '*bold*'

#### Nastaveni editacniho rozhrani #####
$GUIButtonDirUrlFmt = '$PubDirUrl/guiedit'; # slozka s edita�n�mi tla��tky
$EnableGUIButtons = 1; # Zapnuti editacnich tlacitek
# Definice editacnich tlacitek
$GUIButtons['h1'] = array(500, '\\n! ', '\\n', '$[Nadpis1]', '$GUIButtonDirUrlFmt/h11.gif"$[Nadpis1]"');
$GUIButtons['h2'] = array(510, '\\n!! ', '\\n', '$[Nadpis2]', '$GUIButtonDirUrlFmt/h22.gif"$[Nadpis2]"');
$GUIButtons['h3'] = array(520, '\\n!!! ', '\\n', '$[Nadpis3]', '$GUIButtonDirUrlFmt/h33.gif"$[Nadpis3]"');
$GUIButtons['h4'] = array(530, '\\n!!!! ', '\\n', '$[Nadpis4]', '$GUIButtonDirUrlFmt/h44.gif"$[Nadpis4]"');
$GUIButtons['ol'] = array(540, '\\n# ', '\\n', '$[Ordered list]', '$GUIButtonDirUrlFmt/ol.gif"$[Ordered (numbered) list]"');
$GUIButtons['ul'] = array(550, '\\n* ', '\\n', '$[Unordered list]', '$GUIButtonDirUrlFmt/ul.gif"$[Unordered (bullet) list]"');
$GUIButtons['hr'] = array(560, '\\n----\\n', '', '', '$GUIButtonDirUrlFmt/hr.gif"$[Horizontal rule]"');
$GUIButtons['indent'] = array(570, '\\n->', '\\n', '$[Indented text]', '$GUIButtonDirUrlFmt/indent.gif"$[Indented text]"');
$GUIButtons['outdent'] = array(580, '\\n-<', '\\n', '$[Hanging indent]', '$GUIButtonDirUrlFmt/outdent.gif"$[Hanging indent]"');
$GUIButtons['hit'] = array(590, '%green%', '%%', '', '$GUIButtonDirUrlFmt/hightext.gif"$[Highlight the text color\\n(select text, then click here)]"');
$GUIButtons['hib'] = array(600, '%bgcolor=yellow%', '%%', '', '$GUIButtonDirUrlFmt/highbg.gif"$[Highlight the paper color]"');
$GUIButtons['sig'] = array(610, '----\\n~~~:\\\\\\\\\n', '\\n ~~~~\\n\\n', '', '$GUIButtonDirUrlFmt/sig.gif"$[Stamp your name and current date]"');
$GUIButtons['code'] = array(620, "[@", "@]", '$[Code]', '$GUIButtonDirUrlFmt/code.gif"$[Code]"');
$GUIButtons['table'] = array(630, '||border=1 width=80%\\n||!Hdr ||!Hdr ||!Hdr ||\\n||     ||     ||     ||\\n||     ||     ||     ||\\n', '', '', '$GUIButtonDirUrlFmt/table.gif"$[Table]"');

# Definice uzivatelskych stylu s1 - s4:  
$WikiStyle['s1']['background-color']='#def';
$WikiStyle['s1']['padding']='3px';
$WikiStyle['s1']['border']='1px';
$WikiStyle['s2']['background-color']='#efd';
$WikiStyle['s2']['padding']='3px';
$WikiStyle['s3']['background-color']='#ffb';
$WikiStyle['s3']['padding']='3px';
$WikiStyle['s4']['background-color']='#fed';
$WikiStyle['s4']['padding']='3px';

# Definice tlacitek pro uzivatelske styly
$GUIButtons ['style1'] = array(700, '%25block s1%25', '', '',
'$GUIButtonDirUrlFmt/st1.gif"$[Style 1]"');
$GUIButtons ['style2'] = array(710, '%25block s2%25', '', '',
'$GUIButtonDirUrlFmt/st2.gif"$[Style 2]"');
$GUIButtons ['style3'] = array(720, '%25block s3%25', '', '',
'$GUIButtonDirUrlFmt/st3.gif"$[Style 3]"');
$GUIButtons ['style4'] = array(730, '%25block s4%25', '', '',
'$GUIButtonDirUrlFmt/st4.gif"$[Style 4]"');

#$ROSPatterns['/\n*$/'] = '';
#$ROSPatterns['/$/D'] = "\n";
#$TimeFmt = '%B %d, %Y, at %I:%M %p EST';

DisableMarkup('dl');