<?php if (!defined('PmWiki')) exit();
# Copyright 2012 by DevilCraft Studio - pmWiki skin
global $FmtPV, $SkinName, $SkinVersionDate, $SkinVersionNum, $SkinVersion, $SkinRecipeName, 
       $SkinSourceURL, $RecipeInfo;

$RecipeInfo['wikibookSkin']['Version'] = '2013-02-06';
$SkinName = 'wikibook';
$SkinRecipeName = "WikiBook";

global $WikiBookSkin; $WikiBookSkin = 1; 

global $DefaultColor, $DefaultFont, $DefaultPageWidth, 
       $EnableStyleOptions, $EnableThemes, $EnableColorOptions,
       $EnableFontOptions, $EnablePageWidthOptions,
       $EnableGroupTitle, $EnableLeftBarToggle,
       $EnableToggleCookies, $EnableViewSwitcher,$EnablePopupEditForm,
       $EnablePmWikiCoreCss, $LeftWidth, $PageBorder;
       
# Skin je zapotřebí nastavit také v config.php
SDV($DefaultColor,'light'); # barevné vzhledy: light, dark
SDV($DefaultFont, 'arial'); # písma: arial
SDV($DefaultPageWidth, '960'); # šířka wide, 960, 1000, 1300 nebo border 

# Základní šířka levého sloupce
SDV($LeftWidth, '180px');
SDV($PageBorder, '0px');

# Uživatelské nastavení lze vypnout $EnableStyleOptions = 0;
SDV($EnableStyleOptions, 1);

# Zakázání změn za běhu skinu
SDV($EnableColorOptions, 0);
SDV($EnableFontOptions, 0);
SDV($EnablePageWidthOptions, 0); 
SDV($EnableThemes, 1);
SDV($EnableGroupTitle, 0);

# Skrývání sloupců
SDV($EnableLeftBarToggle, 0);
SDV($EnableToggleCookies, 0);

# Přepínač pohledů
SDV($EnableViewSwitcher, 1);

## Náhled v editaci stránky
SDV($EnablePopupEditForm, 1);

# Načíst pmwiki-core.css místo přednastavených stylů
SDV($EnablePmWikiCoreCss, 1);

# Seznam nastavení
global $PageColorList, $PageFontList, $PageWidthList;
SDVA($PageColorList, array (
        'light' => 'wikibook-light.css',
        'dark' => 'wikibook-dark.css',
        ));
SDVA($PageFontList, array ( 
        'arial' => 'font-arial.css',
        ));
SDVA($PageWidthList, array (
        'wide' => '',
        '960' => '960',
        '1000' => '1024',
        '1024' => '1024',
        '1280' => '1280',
        '1300' => '1280',
        'border' => 'border',
        ));

# =========== konec konfigurace skin.php ================= # 

# Kontrola kompatibility skinu
global $VersionNum, $CompatibilityNotice;
if($VersionNum < '2001016') 
   $CompatibilityNotice = "<p style='color:yellow'>Problém s kompatibilitou: Prosím aktualizujte na nejnovější verzy pmwiki</p>";

# pmwiki core styles 
# disable pmwiki default core styles, load from core.css
global $HTMLStylesFmt, $PmWikiCoreStylesFmt;
if($EnablePmWikiCoreCss==1) { 
# awaiting pmwiki suport for pmwiki-core.css, $PmWikiCoreCss may need update!
  global $PmWikiCoreCss;
  SDV($PmWikiCoreCss, "pmwiki-core.css");
  if(file_exists("$FarmD/pub/css/$PmWikiCoreCss")) SDV($PmWikiCoreStylesFmt, "
    <link href='$FarmPubDirUrl/css/$PmWikiCoreCss' rel='stylesheet' type='text/css' />");
  else
    SDV($PmWikiCoreStylesFmt, "
    <link href='$SkinDirUrl/css/pm-core.css' rel='stylesheet' type='text/css' />");
  
   $HTMLStylesFmt['pmwiki'] = '';
   $HTMLStylesFmt['diff'] = '';
   $HTMLStylesFmt['simuledit'] = '';
   $HTMLStylesFmt['markup'] = '';
   $HTMLStylesFmt['urlapprove'] = '';
   $HTMLStylesFmt['vardoc'] = '';
   $HTMLStylesFmt['wikistyles']= '';
}

# check for javascript cookie, set $javascript var for (:if enabled javascript:) switch  
global $javascript;
if (isset($_COOKIE['javascript'])) $javascript = $_COOKIE['javascript']; 

global $ColorCss, $FontCss, $PageWidth;
$sc = $DefaultColor;
$sf = $DefaultFont;
$sw = $DefaultPageWidth;
$ColorCss = $PageColorList[$sc];
$FontCss = $PageFontList[$sf];
$PageWidth = $PageWidthList[$sw];

# add stylechange.php for cookie setting code if set.
if ($EnableStyleOptions == 1) include_once("$SkinDir/stylechange.php");

# šířka sloupců 
global $HTMLStylesFmt;
$HTMLStylesFmt[] = "
  #left-box { width: $LeftWidth; }  
  #right-box { width: $RightWidth; }\n";

## automatic loading of skin default config pages
global $WikiLibDirs, $SkinDir;
    $where = count($WikiLibDirs);
    if ($where>1) $where--;
    array_splice($WikiLibDirs, $where, 0, 
        array(new PageStore("$SkinDir/wikilib.d/\$FullName")));
        
# set default edit form and configuration page
global $XLLangs, $PageEditForm, $SiteGroup; 
SDV($PageEditForm, $SiteGroup.'.Popup-EditForm');
XLPage('dcs', 'Site.dcs-Configuration' );
   array_splice($XLLangs, -1, 0, array_shift($XLLangs)); 
   
# popup editform load switch
global $ShowHide, $SiteGroup, $HTMLHeaderFmt, $action;
if ($action=='edit') {
  if($EnablePopupEditForm==1) {
    if (!$ShowHide) include_once("$SkinDir/showhide.php");
    $HTMLHeaderFmt['popupedit'] = "
      <script type='text/javascript'><!--
         document.write(\"<link href='$SkinDirUrl/css/popup2edit.css' rel='stylesheet' type='text/css' />\");
      --></script>
      <noscript>
         <link href='$SkinDirUrl/css/popup2edit-noscript.css' rel='stylesheet' type='text/css' />
      </noscript>
      ";
  }
  else $HTMLHeaderFmt['popupedit'] = "
    <link href='$SkinDirUrl/css/popup2edit-noscript.css' rel='stylesheet' type='text/css' />";
} 

   
# load views script if enabled
if($EnableViewSwitcher==1) {
    $ViewList['big'] = 'big'; # add 'big' to list of view keywords
    SDV($ViewCookie, $CookiePrefix.$SkinName.'_setview');
    include_once("$SkinDir/views.php"); # load views script
  # set inits for 'big' view 
    global $FontSizeDefault, $FontSizeIncrement, $RTInit, $LTInit;
    if($View=='big') {
             $FontSizeDefault = '120'; # base size percentage
             $FontSizeIncrement = '10';      # size increment in percent
             $RShow = '0';            # no right bar initially
             $LShow = '1';            # (0=no) left bar initially
    };
};

# set dcsSkin as global variable for (:if enable dcsSkin:) markup 
global $WikiBookSkin;
$WikiBookSkin = 1;

# add {$PageLogoUrl} to page variables to use on default PageHeader page
global $FmtPV;
$FmtPV['$PageLogoUrl'] = '$GLOBALS["PageLogoUrl"]';

# do not show topmenu bar if PageTopMenu is empty
$gtm = FmtPageName('$Group.PageTopMenu',$pagename);
$stm = FmtPageName('$SiteGroup.PageTopMenu',$pagename);
if (PageExists($gtm)) $page = ReadPage($gtm);
if (@$page['text']=='') $nogtm = 1; 
if (PageExists($stm)) $page = ReadPage($stm);
if (@$page['text']=='') $nostm = 1; 
if ($nogtm==1 && $nostm==1){ 
#        SetTmplDisplay('PageTopMenuFmt',0);
       };


## use alternative searchbox markup
include_once("$SkinDir/searchbox2.php");
  

# PageVariables for group links Cluster support (see Cookbook.Cluster)
# Support for Cluster and Hg recipe through $Cfn naming the PV function of each recipe 
$Cfn = array('ClusterPageName','HierarchicalPagenames');
foreach($Cfn as $fn) { 
  if (function_exists($fn)) { 
    $FmtPV['$PageHeader'] = $fn.'($group, "PageHeader")';
    $FmtPV['$TitleBar'] = $fn.'($group, "TitleBar")';
    $FmtPV['$PageTopMenu'] = $fn.'($group, "PageTopMenu")';
    $FmtPV['$SideBar'] = $fn.'($group, "SideBar")';
    $FmtPV['$RightBar'] = $fn.'($group, "RightBar")';
    $FmtPV['$PageFootMenu'] = $fn.'($group, "PageFootMenu")';
    $FmtPV['$PageFooter'] = $fn.'($group, "PageFooter")';
    $FmtPV['$PageSideBarFooter'] = $fn.'($group, "PageSideBarFooter")'; 
    }
}
 
## set var $RBExists if RightBar exists
if ($EnableRightBar==1 || $EnableMarkupShowRight==1) {
  $pageRB = FmtPageName('$FullName-RightBar',$pagename);
  if (PageExists($pageRB))  $RBExists = 1;
  $clusterRB = FmtPageName('{$RightBar}',$pagename); //used with Cluster or Hg 
  if (PageExists($clusterRB)) $RBExists = 1;
  $groupRB = FmtPageName('$Group.RightBar',$pagename);
  if (PageExists($groupRB))  $RBExists = 1;
  $siteRB = FmtPageName('$SiteGroup.RightBar',$pagename);
  if (PageExists($siteRB))  $RBExists = 1;
}

# add left bar toggle switches if enabled
if($EnableLeftBarToggle==1) {
    include_once("$SkinDir/togglebars.php"); }

# changes to extended markup recipe for selflink definition:
global $LinkPageSelfFmt;
#$LinkPageSelfFmt = "<a class='selflink'>\$LinkText</a>";

# set HTML title
global $HTMLTitleFmt, $WikiTitle;
$title = PageVar($pagename,'$Title');
$group = PageVar($pagename,'$Group');
SDV($HTMLTitleFmt, "$WikiTitle - $group - $title");

// adding switch for 'Pagename-TitleBar' subpage for fancy font titlebars
$ftb = FmtPageName('$FullName-TitleBar',$pagename);
if(PageExists($ftb))  $HTMLStylesFmt[] = " .titlelink { display:none } \n ";

##========== Special Markups ==========##

## markup (:noleft:)
function NoLeft2() {
    global $LeftToggleFmt;
    $LeftToggleFmt = ""; 
    SetTmplDisplay('PageLeftFmt',0);
    return ''; }
Markup('noleft','directives','/\\(:noleft:\\)/e', 
    "NoLeft2()");

## Markup (:notopmenu:)
function NoTopMenu2() {
    global $HTMLStylesFmt;
    SetTmplDisplay('PageTopMenuFmt',0);
    $HTMLStylesFmt[] = "
         #header {border-bottom:1px solid #003466}\n ";   
    return ''; }
Markup('notopmenu','directives','/\\(:notopmenu:\\)/e', 
   "NoTopMenu2()");

## Markup (:nofootmenu:) 
Markup('nofootmenu','directives','/\\(:nofootmenu:\\)/e',
  "SetTmplDisplay('PageFootMenuFmt', 0)");
  
## Markup (:noaction:)
function NoAction2() {
    global $HTMLStylesFmt;
    SetTmplDisplay('PageFootMenuFmt', 0);
    SetTmplDisplay('PageTopMenuFmt',0);
    $HTMLStylesFmt['noaction'] = "
         #header {border-bottom:1px solid #003466}\n ";   
    return ''; }
Markup('noaction','directives','/\\(:noaction:\\)/e',
  "NoAction2()");
  
## Markup (:noheader:) 
Markup('noheader','directives','/\\(:noheader:\\)/e', 
    "SetTmplDisplay('PageHeaderFmt', 0)");
    
## Markup (:notitle:) 
Markup('notitle','directives','/\\(:notitle:\\)/e', 
    "SetTmplDisplay('PageTitleFmt', 0)");
    
## Markup (:nogroup:) 
Markup('nogroup','directives','/\\(:nogroup:\\)/e', 
    "NoGroupTitle($pagename)");
function NoGroupTitle($pagename) {
    global $HTMLStylesFmt;
    $HTMLStylesFmt['nogrouptitle'] = 
    "  .pagegroup {display:none;} \n ";
}
# display or hide group-link in titlebar.
if($EnableGroupTitle==0) NoGroupTitle($pagename);
    
## Markup (:fullpage:)
function FullPage() {
     SetTmplDisplay('PageHeaderFmt', 0);         
     SetTmplDisplay('PageTopMenuFmt',0);
     SetTmplDisplay('PageFootMenuFmt', 0);
     SetTmplDisplay('PageFooterFmt', 0);    
     SetTmplDisplay('PageLeftFmt',0);
     SetTmplDisplay('PageRightFmt',0);  
     return '';
}
Markup('fullpage','directives','/\\(:fullpage:\\)/e',
  "FullPage()");
  
## Markup (:theme colorname fontname:)
function SetTheme($sc,$sf) {
   global $ColorCss,$PageColorList,$FontCss,$PageFontList,$HTMLHeaderFmt,$SkinDirUrl,$EnableThemes;
   if (@$PageColorList[$sc]) { 
      $ColorCss = $PageColorList[$sc];
      $HTMLHeaderFmt['skin-color'] = "   
   <link href='$SkinDirUrl/css/$ColorCss' rel='stylesheet' type='text/css' media='screen' />";
   }
   if($sf) {
     if (@$PageFontList[$sf]) {
       $FontCss = $PageFontList[$sf];};
       $HTMLHeaderFmt['skin-font'] = "   
   <link href='$SkinDirUrl/css/$FontCss' rel='stylesheet' type='text/css' media='screen' />";
   }
};
if($EnableThemes == 1) {
    Markup('theme', 'fulltext',
      '/\\(:theme\\s+([-\\w]+)\\s*([-\\w]*)\\s*:\\)/e',
      "PZZ(SetTheme('$1', '$2'))"); 
}
else {
    Markup('theme', 'fulltext',
      '/\\(:theme\\s+([-\\w]+)\\s*([-\\w]*)\\s*:\\)/e',
      "");
};
  
## add double line horiz rule markup ====
Markup('^====','>^->','/^====+/','<:block,1>
  <hr class="hr-double" />');
  
## removing header, title for history and uploads windows
global $action;
if ($action=='diff' || $action=='upload') { 
           
            SetTmplDisplay('PageHeaderFmt', 0);
            SetTmplDisplay('PageTitleFmt', 0);
    };

## alternative Diff (History) form with link in title
global $PageDiffFmt, $PageUploadFmt;
$PageDiffFmt = "<h3 class='wikiaction'>
  <a href='\$PageUrl'> \$FullName</a> $[History]</h3>
  <p>\$DiffMinorFmt - \$DiffSourceFmt - <a href='\$PageUrl'> $[Cancel]</a></p>";

## alternative Uploads form with link in title 
$PageUploadFmt = array("
  <div id='wikiupload'>
  <h3 class='wikiaction'>$[Attachments for] 
  <a href='\$PageUrl'> {\$FullName}</a></h3>
  <h3>\$UploadResult</h3>
  <form enctype='multipart/form-data' action='{\$PageUrl}' method='post'>
  <input type='hidden' name='n' value='{\$FullName}' />
  <input type='hidden' name='action' value='postupload' />
    <p align='right' style='float:left'>$[File to upload:]
    <input name='uploadfile' type='file' size=50 /></p>
    <p align='right' style='float:left' />$[Name attachment as:]
    <input type='text' name='upname' value='\$UploadName' size=25 />
    <input type='submit' value=' $[Upload] ' /></p>
    </form></div><br clear=all />",
  'wiki:$[{$SiteGroup}/UploadQuickReference]');
  
global $SkinColor;
## nastavení proměnných stránky
$SkinVersionDate = $RecipeInfo['dcsSkin']['Version'];
$SkinVersionNum = str_replace("-","",$SkinVersionDate);
$SkinVersion = $SkinName." ".$SkinVersionDate;
$SkinSourceUrl = 'http://www.pmwiki.org/wiki/Cookbook/'.$SkinRecipeName;
$SkinColor = $sc;
# nastavení proměnných jako proměnné stránky
$FmtPV['$SkinName'] = '$GLOBALS["SkinName"]';
$FmtPV['$SkinVersionDate'] = '$GLOBALS["SkinVersionDate"]';
$FmtPV['$SkinVersionNum'] = '$GLOBALS["SkinVersionNum"]';
$FmtPV['$SkinVersion'] = '$GLOBALS["SkinVersion"]';
$FmtPV['$SkinRecipeName'] = '$GLOBALS["SkinRecipeName"]';
$FmtPV['$SkinSourceUrl'] = 'PUE($GLOBALS["SkinSourceUrl"])';
$FmtPV['$SkinDirUrl'] = 'PUE($GLOBALS["SkinDirUrl"])';
$FmtPV['$SkinColor'] = 'PUE($GLOBALS["SkinColor"])';

# přidá zpětnou kompatibilitu pro non-relative urls
if ($VersionNum < 2001900) 
      Markup('{*$var}', '<{$var}', '/\\{\\*\\$/', '{$');

# přídá skin šablony do stránky
global $HTMLHeaderFmt;
$HTMLHeaderFmt['skin-layout'] = "
   <link href='$SkinDirUrl/css/layout-wikibook.css' rel='stylesheet' type='text/css' />
   <link href='$SkinDirUrl/css/layout-main.css' rel='stylesheet' type='text/css' />
   <link href='$SkinDirUrl/css/layout-print.css' rel='stylesheet' type='text/css' media='print' />";  
$HTMLHeaderFmt['skin-font'] = "  
   <link href='$SkinDirUrl/css/$FontCss' rel='stylesheet' type='text/css' media='screen' />";
$HTMLHeaderFmt['skin-color'] = "   
   <link href='$SkinDirUrl/css/$ColorCss' rel='stylesheet' type='text/css' media='screen' />
   ";

# nastavení šířky stránky
if ($sw=='960') { $HTMLHeaderFmt['pagewidth'] = " 
    <style>
    body {padding:10px 0;} \n
    #outer-box {width:960px; height:90%;}
    </style>
    ";}
if ($sw=='1000'||$sw=='1024') { $HTMLHeaderFmt['pagewidth'] = " 
    <style>
    body {padding:10px 0;} \n
    #outer-box {width:1004px; height:100%;}
    </style>
    ";}
if ($sw=='1300'||$sw=='1280') { $HTMLHeaderFmt['pagewidth'] = " 
    <style>
    #outer-box {width:1260px; height:100%;}
    </style>
    ";}
if ($sw=='border') { $HTMLHeaderFmt['pagewidth'] = " 
    <style>
    body {padding:$PageBorder;}
    </style>
    ";}