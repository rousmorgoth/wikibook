<?php if (!defined('PmWiki')) exit();

/*  Copyright 2006 Hans Bracker. 
    This file is togglebars.php; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.
    
    togglebars.php is part of dcs skin for pmwiki 2
    
    changes: 
    2006-08-21: added togglebar cookie settings
  
*/ 
# defined in skin.php:
#global $EnableToggleCookies;
#SDV($EnableToggleCookies, 1);

# left toggle switch defaults: 1=show switch
SDV($LShow, 1); 

# check cookies
global $LShowCookie, $CookiePrefix;
SDV($LShowCookie, $CookiePrefix.$SkinName.'_setLshow');;
if ($EnableToggleCookies==1 && isset($_COOKIE[$LShowCookie])) $LShow = $_COOKIE[$LShowCookie];


# load toggle script
global $HTMLHeaderFmt,  $HTMLStylesFmt; 
$HTMLHeaderFmt['showhide'] = "
<script type='text/javascript' language='JavaScript1.2'>
    var toggleCookies = '$EnableToggleCookies';
    var lcookie = '$LShowCookie';
    var rcookie = '$RShowCookie';
    var lshow = '$LShow';
    var rshow = '$RShow';
    var show = '$[Show]';
    var hide = '$[Hide]';
    var lwidth = '$LeftWidth';
    var rwidth = '$RightWidth';
</script>
<script type='text/javascript' language='JavaScript1.2' src='$SkinDirUrl/togglebars.js'></script>
";

## define LeftToggle
global $LeftToggleFmt;
if($EnableLeftBarToggle==0) $LeftToggleFmt = ""; 
if($EnableLeftBarToggle==1)  { 
 if($LShow==1) {
   $LeftToggleFmt = "
   <script type='text/javascript' language='JavaScript1.2'><!--
   if (toggleLeft) document.write(\"<input name='lb' type='button' class='togglebox' value='\$[Hide] &darr;' onclick='toggleLeft()' />\") 
   --></script>
   ";}

 if($LShow==0) {
   $HTMLStylesFmt[] = " #sidebar {display:none} #left-box {width:1px} ";
   $LeftToggleFmt = "
   <script type='text/javascript' language='JavaScript1.2'><!--
   if (toggleLeft) document.write(\"<input name='lb' type='button' class='togglebox' value='\$[Show] &darr;' onclick='toggleLeft()' />\")
   --></script>
   ";}
 }