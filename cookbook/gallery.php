<?php
/**
 * Bildergalerie innerhalb von PmWiki anzeigen
 *
 * @author Stefan Jahn <stefan.jahn@nemesis-sektor.de>
 * @copyright Copyright (C) 2004 Stefan Jahn <stefan.jahn@nemesis-sektor.de>
 * @license  http://www.gnu.org/licenses/gpl.html GNU General Public License
 * @version 20050508
 */


/*
 * Copyright (C) 2004 Stefan Jahn <stefan.jahn@nemesis-sektor.de>
 *
 * gallery.php is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * SJ Websystem is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/*
 * Modified by Chris Cox
 * Include support for a text file called <suffix>_.txt in
 * the pictures directory which holds one line descriptions
 * for the pictures.  First picture corresponds to the text
 * in line 1, Second picture is line 2, and so on.
 * 
 * Search for Chris Cox in the file to find the modifications made.
 */

Markup('gallery','style','/\\(:gallery( (\\S+))?:\\)/e','gallery(\'$1\')');

// EXIF- und IPTC-Funktionen an-/abschalten
SDV($gallery_exif, TRUE);
SDV($gallery_iptc, False);

// Nabivation an-/abschalten
SDV($gallery_navigationtop, TRUE);
SDV($gallery_navigationbottom, False);

// Kommentare an-/abschalten
SDV($gallery_comment, False);
SDV($gallery_commentgroup, 'Main');

// Texte
/*
SDV($gallery_nouploads, 'Noch keine Anh�nge in dieser Gruppe vorhanden.');
SDV($gallery_nopictures, 'Es sind noch keine Bilder vorhanden.');
SDV($gallery_picturename, 'Bild #');
SDV($gallery_of, 'von');
SDV($gallery_content, '�bersicht');
SDV($gallery_upload, 'Bild hochladen');
SDV($gallery_originalpicture, 'Originalbild');
SDV($gallery_city, 'Ort');
SDV($gallery_country, 'Land');
SDV($gallery_viewcomment, 'Kommentar lesen/schreiben');
*/

// Please remove remarks for english translation
SDV($gallery_nouploads, 'Tato skupina je bez obr�zk�.');
SDV($gallery_nopictures, 'Obr�zky nenalezeny.');
SDV($gallery_picturename, 'Obr�zek �.');
SDV($gallery_of, '/celkem');
SDV($gallery_content, 'N�hled');
SDV($gallery_upload, 'Upload obr�zku');
SDV($gallery_originalpicture, 'Obr�zek');
SDV($gallery_city, 'M�sto');
SDV($gallery_country, 'St�t');
SDV($gallery_viewcomment, 'Zobraz/Edituj koment��');

SDV($gallery_titlestart, '<h4><p style="text-align: center;">');
SDV($gallery_titleend, '</p></h4>');

/**
 * Hauptroutine - Entweder Galerie oder einzelnes Bild anzeigen
 */
function gallery($parameter) {
	global $pagename;
	global $UploadDir;
	global $UploadUrlFmt;

	global $gallery_nouploads;
	global $gallery_nopictures;
	global $gallery_upload;
	// Chris Cox, added g_map support
	global $g_map;

	$parameter = strtolower(trim($parameter));
	
	$output = '';
	
	// Pfad bestimmen
	$server = 'http://'.$_SERVER['SERVER_NAME'].'/';
	$directory = str_replace($server, '', $UploadUrlFmt);
	$directory = str_replace($UploadDir, '', $directory);
	$script = FmtPageName('$PageUrl',$pagename);
	$pic = $_GET['pic'];

	// Wiki-Werte einlesen
	$array = explode('.', $pagename);
	$path = $UploadDir.'/'.$array[0];
	$url = $UploadUrlFmt.'/'.$array[0].'/';
	$name = strtolower($array[1]);
	$suffix = $name.'_';

	// Pr�fen ob Verzeichnis vorhanden ist
	if (!is_dir($path)) {
		$output.= $gallery_nouploads."\n";
		if ($gallery_upload != '') $output.= '<a href="'.$script.'?action=upload&amp;upname='.$suffix.'" title="'.$gallery_upload.'">'.$gallery_upload.'</a>'."\n";
		return $output;
	}

	$path.= '/';

	// Chris Cox, populate g_map array from <suffix>_.txt
	$g_map_file = $path.$suffix.".txt";
  	if (file_exists($g_map_file)) {
    		$fp = @fopen($g_map_file, "r");
		$i = 1;
		while (!feof($fp)) {
			$comment = fgets($fp);
			$g_map[$i] = $comment;
//			$output = $output . " " . "g_map[" . $i ."] = $comment";
			$i++;
		}
		fclose($fp);
	}


	// Verzeichnis einlesen
	$array = gallery_readdir($path, $suffix);
	$image = $array[1];
	$thumbnail = $array[2];
	$original = $array[3];
	$name = $array[4];

	// Bilder z�hlen
	$max = count($image);

	// Pr�fen ob Bilder vorhanden sind
	if ($max == 0) {
		$output.= $gallery_nopictures."\n";
		if ($gallery_upload != '') $output.= '<a href="'.$script.'?action=upload&amp;upname='.$suffix.'" title="'.$gallery_upload.'">'.$gallery_upload.'</a>'."\n";
		return $output;
	}

	// Parameter pr�fen
	if (isset($pic)) {
		if ($pic > $max) $pic = $max;
		if ($pic < 1) $pic = 1;
	}

	// Gallerie oder einzelnes Bild anzeigen
	if ($parameter == 'all') {
		$output.= gallery_displayall($image, $directory, $suffix, $max);
	} else {
		if (!isset($pic)) $output.= gallery_displaythumbnails($thumbnail, $image, $directory, $suffix, $max);
		else $output.= gallery_displayimage($image, $name, $directory, $suffix, $max, $pic, $original);
	}

	return $output;
}


/**
 * Thumbnail erzeugen
 *
 * @param string $image Dateiname des Bildes
 * @param string $thumbnail Dateiname des Thumbnails
 */
function gallery_createthumbnail($image, $thumbnail) {
	// Infos �ber Bild holen
	$info = getimagesize($image);
	$width = $info[0];
	$height = $info[1];
	$type = $info[2];

	// Thumbnail erstellen
	if ($type == 1) $new = imagecreatefromgif($image);
	elseif ($type == 2) $new = imagecreatefromjpeg($image);
	elseif ($type == 3) $new = imagecreatefrompng($image);

	$newwidth = 100;
	$newheight = $height/($width/$newwidth);
	$newimage = imagecreatetruecolor($newwidth, $newheight);
	imagecopyresampled($newimage, $new, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

	if ($type == 1) imagegif($newimage, $thumbnail);
	elseif ($type == 2) imagejpeg($newimage, $thumbnail);
	elseif ($type == 3) imagepng($newimage, $thumbnail);
}


/**
 * Navigation anzeigen
 *
 * @param integer $pic Aktuelle Bild
 * @param integer $max Anzahl der Bilder
 */
function gallery_navigation($pic, $max) {
	global $pagename;
	
	global $gallery_picturename;
	global $gallery_content;

	$output = '';
	
	$script = FmtPageName('$PageUrl',$pagename);

	if ($pic > 1) {
		$output.= '<a href="'.$script.'?pic=1" title="'.$gallery_picturename.'1">&lt;&lt;</a>&nbsp;'."\n";
		$output.= '<a href="'.$script.'?pic='.($pic-1).'" title="'.$gallery_picturename.($pic-1).'">&lt;</a>&nbsp;'."\n";
	} else {
		$output.= '&lt;&lt;&nbsp;'."\n";
		$output.= '&lt;&nbsp;'."\n";
	}

	$output.= '<a href="'.$script.'" title="'.$gallery_content.'">'.$gallery_content.'</a>&nbsp;'."\n";

	if ($pic < $max) {
		$output.= '<a href="'.$script.'?pic='.($pic+1).'" title="'.$gallery_picturename.($pic+1).'">&gt;</a>&nbsp;'."\n";
		$output.= '<a href="'.$script.'?pic='.($max).'" title="'.$gallery_picturename.($max).'">&gt;&gt;</a>'."\n";
	} else {
		$output.= '&gt;&nbsp;'."\n";
		$output.= '&gt;&gt;'."\n";
	}

	return $output;
}


/**
 * Verzeichnis einlesen
 *
 * @param string $path Pfad zum Upload-Verzeichnis der aktuellen Seite des Wikis
 * @param string $suffix Titel der aktuellen Seite des Wikis
 * @return array
 */
function gallery_readdir($path, $suffix) {
	$dir = dir($path);
	while ($filename = $dir->read()) {
		if (!stristr($filename, 'thumb_') and stristr($filename, $suffix) and !stristr($filename, 'original_')) {
			if (stristr($filename, '.jpg')) {
				$image[] = $path.$filename;
				$name[] = str_replace('.jpg', '', $filename);
				$thumbnail[] = $path.'thumb_'.$filename;
				$original[] = $path.'original_'.$filename;
			}
			if (stristr($filename, '.png')) {
				$image[] = $path.$filename;
				$name[] = str_replace('.png', '', $filename);
				$thumbnail[] = $path.'thumb_'.$filename;
				$original[] = $path.'original_'.$filename;
			}
			if (stristr($filename, '.gif')) {
				$image[] = $path.$filename;
				$name[] = str_replace('.gif', '', $filename);
				$thumbnail[] = $path.'thumb_'.$filename;
				$original[] = $path.'original_'.$filename;
			}
		}
	}
	$dir->close();

	// Inhaltsverzeichnis sortieren
	if ($image) sort($image);
	if ($thumbnail) sort($thumbnail);
	if ($original) sort($original);
	if ($name) sort($name);

	$array[1] = $image;
	$array[2] = $thumbnail;
	$array[3] = $original;
	$array[4] = $name;

	return $array;
}


/**
 * Thumbnails anzeigen
 *
 * @param array $thumbnail Dateinamen der Thumbnails
 * @param array $image Dateinamen der Bilder
 * @param array $directory Wiki-Verzeichnis
 * @param string $suffix Titel der aktuellen Seite des Wikis
 * @param integer $max Anzahl der Bilder
 */
function gallery_displaythumbnails($thumbnail, $image, $directory, $suffix, $max) {
	global $pagename;
	
	global $gallery_picturename;
	global $gallery_iptc;
	// Chris Cox, added g_map support
	global $g_map;

	$output = '';
	
	$script = FmtPageName('$PageUrl',$pagename);

	$output.= '<div style="text-align:center">'."\n";
	for ($i = 0; $i < $max; $i++):
		// Thumbnail erzeugen falls nicht vorhanden ist
		if (!file_exists($thumbnail[$i])) gallery_createthumbnail($image[$i], $thumbnail[$i]);

		$size = getimagesize($thumbnail[$i]);
		getimagesize($image[$i], $info);

		// IPTC-Daten ermitteln
		if (isset($info["APP13"]) and $gallery_iptc) {
			$iptc = iptcparse($info['APP13']);
			$iptc_caption = trim(htmlentities($iptc['2#120'][0]));
			if ($iptc_caption != '') {
				$iptc_caption = str_replace("\r",'',$iptc_caption);
				$iptc_caption = str_replace("\n",' / ',$iptc_caption);
				$iptc_caption = ' - '.$iptc_caption;
			}
		} else unset($iptc_caption);

		// Thumbnail anzeigen
		// Chris Cox, Try g_map name first if present for title and alt text.
		$g_alttext = $g_map[$i+1];
		if (! $g_alttext) {
			$g_alttext = $gallery_picturename.($i+1).$iptc_caption;
		}
		$output.= '<a href="'.$script.'?pic='.($i+1).'" title="'.$g_alttext.'">';
		$output.= '<img class="thumb" src="/'.str_replace('%2F', '/', urlencode($directory.$thumbnail[$i])).'" alt="'.$g_alttext.'" '.$size[3].' />';
		$output.= '</a>'."\n";

	endfor;
	$output.= '</div>'."\n";

	$output.= gallery_linkupload($suffix);
	
	return $output;
}


/**
 * Alle Bilder anzeigen
 *
 * @param array $image Dateinamen der Bilder
 * @param array $directory Wiki-Verzeichnis
 * @param string $suffix Titel der aktuellen Seite des Wikis
 * @param integer $max Anzahl der Bilder
 */
function gallery_displayall($image, $directory, $suffix, $max) {
	global $pagename;
	
	global $gallery_picturename;
	global $gallery_iptc;

	$output = '';
	
	$script = FmtPageName('$PageUrl',$pagename);

	$output.= '<div style="text-align:center">'."\n";
	for ($i = 0; $i < $max; $i++):
		$size = getimagesize($image[$i]);
		getimagesize($image[$i], $info);

		// IPTC-Daten ermitteln
		if (isset($info["APP13"]) and $gallery_iptc) {
			$iptc = iptcparse($info['APP13']);
			$iptc_caption = trim(htmlentities($iptc['2#120'][0]));
			if ($iptc_caption != '') {
				$iptc_caption = str_replace("\r",'',$iptc_caption);
				$iptc_caption = str_replace("\n",' / ',$iptc_caption);
				$iptc_caption = ' - '.$iptc_caption;
			}
		} else unset($iptc_caption);

		// Bild anzeigen
		$output.= '<img class="photo" src="/'.str_replace('%2F', '/', urlencode($directory.$image[$i])).'" alt="'.$gallery_picturename.($i+1).$iptc_caption.'" '.$size[3].' />'."\n";
	endfor;
	$output.= '</div>'."\n";

	$output.= gallery_linkupload($suffix);
	
	return $output;
}


/**
 * Bild anzeigen
 *
 * @param array $image Dateinamen der Bilder
 * @param array $directory Wiki-Verzeichnis
 * @param string $suffix Titel der aktuellen Seite des Wikis
 * @param integer $max Anzahl der Bilder
 * @param integer $pic Nummer des aktuellen Bildes
 * @param array $image Dateinamen der original Bilder
 */
function gallery_displayimage($image, $name, $directory, $suffix, $max, $pic, $original) {
	global $gallery_picturename;
	global $gallery_of;
	global $gallery_titlestart;
	global $gallery_titleend;
	global $gallery_originalpicture;
	global $gallery_city;
	global $gallery_country;
	global $gallery_exif;
	global $gallery_iptc;
	global $gallery_navigationtop;
	global $gallery_navigationbottom;
	global $gallery_comment;
	global $gallery_commentgroup;
	global $gallery_viewcomment;
	// Chris Cox, g_map support
	global $g_map;

	$output = '';
	
	$size = getimagesize($image[$pic1], $info);

	// EXIF-Daten ermitteln
	if ($gallery_exif and stristr($image[$pic1], '.jpg')) {
		$exif = exif_read_data($image[$pic1]);
		$exif_date = trim(htmlentities($exif['DateTimeDigitized']));
		$exif_make = trim(htmlentities($exif['Make']));
		$exif_camera = trim(htmlentities($exif['Model']));
		$exif_aperturefnumber = trim(htmlentities($exif['COMPUTED']['ApertureFNumber']));
		$exif_iso = trim(htmlentities($exif['ISOSpeedRatings']));
		if ($exif_iso) $exif_iso = 'ISO '.$exif_iso;
		$exif_focallength = trim(htmlentities($exif['FocalLength']));
		if ($exif_focallength) {
			list($a, $b) = split('/', $exif_focallength);
			$exif_focallength = $a/$b;
			$exif_focallength.= ' mm';
		}
		$exif_exposuretime = trim(htmlentities($exif['ExposureTime']));
		if ($exif_exposuretime) $exif_exposuretime.=' s';
	} else {
		unset($exif_date);
		unset($exif_make);
		unset($exif_camera);
		unset($exif_aperturefnumber);
		unset($exif_iso);
		unset($exif_focallength);
		unset($exif_exposuretime);
	}

	// IPTC-Daten ermitteln
	if (isset($info["APP13"]) and $gallery_iptc) {
		$iptc = iptcparse($info['APP13']);
		$iptc_program = trim(htmlentities($iptc['2#065'][0]));
		$iptc_jobid = trim(htmlentities($iptc['2#022'][0]));
		$iptc_status = trim(htmlentities($iptc['2#007'][0]));
		$iptc_copyright = trim(htmlentities($iptc['2#116'][0]));
		$iptc_specialinstructions = trim(htmlentities($iptc['2#040'][0]));
		$iptc_headline = trim(htmlentities($iptc['2#105'][0]));
		$iptc_captionwriter = trim(htmlentities($iptc['2#122'][0]));
		$iptc_caption = trim(htmlentities($iptc['2#120'][0]));
		$iptc_originaltransmissionreference = trim(htmlentities($iptc['2#103'][0]));
		$iptc_countrycode = trim(htmlentities($iptc['2#100'][0]));
		$iptc_country = trim(htmlentities($iptc['2#101'][0]));
		$iptc_state = trim(htmlentities($iptc['2#095'][0]));
		$iptc_sublocation = trim(htmlentities($iptc['2#092'][0]));
		$iptc_city = trim(htmlentities($iptc['2#090'][0]));
		$iptc_objectname = trim(htmlentities($iptc['2#005'][0]));
		$iptc_source = trim(htmlentities($iptc['2#115'][0]));
		$iptc_credit = trim(htmlentities($iptc['2#110'][0]));
		$iptc_bylinetitle = trim(htmlentities($iptc['2#085'][0]));
		$iptc_byline = trim(htmlentities($iptc['2#080'][0]));
		
		$iptc_caption = str_replace("\r",'',$iptc_caption);
		$iptc_caption = str_replace("\n",'<br />',$iptc_caption);
	} else {
		unset($iptc_program);
		unset($iptc_jobid);
		unset($iptc_status);
		unset($iptc_copyright);
		unset($iptc_specialinstructions);
		unset($iptc_headline);
		unset($iptc_captionwriter);
		unset($iptc_caption);
		unset($iptc_originaltransmissionreference);
		unset($iptc_countrycode);
		unset($iptc_country);
		unset($iptc_state);
		unset($iptc_sublocation);
		unset($iptc_city);
		unset($iptc_objectname);
		unset($iptc_source);
		unset($iptc_credit);
		unset($iptc_bylinetitle);
		unset($iptc_byline);
	}

	$output.= $gallery_titlestart.$gallery_picturename.($pic).' '.$gallery_of.' '.$max.$gallery_titleend."\n";

	$output.= '<div style="text-align:center">'."\n";

	// Navigation anzeigen
	if ($gallery_navigationtop) {
		$output.= gallery_navigation($pic, $max);
		$output.= '<br />'."\n";
	}

	// Bild anzeigen
	$output.= '<img class="photo" src="/'.str_replace('%2F', '/', urlencode($directory.$image[$pic-1])).'" alt="'.$gallery_picturename.($pic).' '.$gallery_of.' '.$max.'" '.$size[3].' />'."\n";
	$output.= '<br />'."\n";

	// EXIF-Datum anzeigen
	if ($gallery_exif and stristr($image[$pic-1], '.jpg')) {
		if ($exif_date != '') {
			$output.= $exif_date."\n";
			$output.= '<br />'."\n";
		}
	}

	// Chris Cox, set caption to g_map entry if present.
	$iptc_caption = $g_map[$pic];

	// Beschreibung anzeigen
	if ($gallery_iptc) {
		$check_output = FALSE;
		if ($iptc_caption != '') {
			$output.= $iptc_caption."\n";
			$output.= '<br />'."\n";
			$check_output = TRUE;
		}
		if ($iptc_city != '') {
			$output.= $gallery_city.': '.$iptc_city;
			if ($iptc_country != '') $output.= ', '.$iptc_country;
			$output.= '<br />'."\n";
			$check_output = TRUE;
		} elseif ($iptc_country != '') {
			$output.= $gallery_country.': '.$iptc_country."\n";
			$output.= '<br />'."\n";
			$check_output = TRUE;
		}
		if ($iptc_sublocation != '') {
			$output.= $iptc_sublocation."\n";
			$output.= '<br />'."\n";
			$check_output = TRUE;
		}
		if ($check_output) $output.= '<br />'."\n";
	}

	// EXIF-Daten anzeigen
	if ($gallery_exif and stristr($image[$pic-1], '.jpg')) {
		unset($exif_string);
		if ($exif_make != '') $exif_string = $exif_make;
		if ($exif_camera != '') {
			if ($exif_string) $exif_string.= ' | ';
			$exif_string.= $exif_camera;
		}
		if ($exif_iso != '') {
			if ($exif_string) $exif_string.= ' | ';
			$exif_string.= $exif_iso;
		}
		if ($exif_exposuretime != '') {
			if ($exif_string) $exif_string.= ' | ';
			$exif_string.= $exif_exposuretime;
		}
		if ($exif_aperturefnumber != '') {
			if ($exif_string) $exif_string.= ' | ';
			$exif_string.= $exif_aperturefnumber;
		}
		if ($exif_focallength != '') {
			if ($exif_string) $exif_string.= ' | ';
			$exif_string.= $exif_focallength;
		}
		if ($exif_string) $output.= '<p>'.$exif_string.'</p>'."\n";
	}

	// Navigation anzeigen
	if ($gallery_navigationbottom) {
		$output.= gallery_navigation($pic, $max);
		$output.= '<br />'."\n";
	}

	if (($gallery_comment) and ($gallery_commentgroup != '')) {
		$pagename = gallery_wikify($name[$pic1]);
		$output.= '<a href="/index.php/'.urlencode($gallery_commentgroup.'.'.$pagename).'" title="'.$gallery_viewcomment.'">'.$gallery_viewcomment.'</a>'."\n";
		$output.= '<br />'."\n";
	}

	if (file_exists($original[$pic1])) {
		$output.= '<a href="/'.str_replace('%2F', '/', urlencode($original[$pic-1])).'" title="'.$gallery_picturename.($pic).'" target="_blank">'.$gallery_originalpicture.'</a>'."\n";
	}

	$output.= '</div>'."\n";

	return $output;
}


/**
 * Sonderzeichen aus Text entfernen
 *
 * @param string $text Text
 * @return string
 */
function gallery_wikify($text) {
	$text = preg_replace('/[^-\\w ]/', '', $text);
	$text = preg_replace('/_+/', '-', $text);
	$text = preg_replace('/--+/', '-', $text);
	$text = preg_replace('/\\s+/', '', $text);

	return $text;
}


/**
 * Link f�r Upoloads anzeigen
 *
 * @param string $suffix Titel der aktuellen Seite des Wikis
 * @return string
 */
function gallery_linkupload($suffix) {
	global $pagename;
	
	global $gallery_upload;

	if ($gallery_upload != '') {
		$script = FmtPageName('$PageUrl',$pagename);
		$output = '<div style="text-align:right">'."\n";
		$output.= '<a href="'.$script.'?action=upload&amp;upname='.$suffix.'" title="'.$gallery_upload.'">'.$gallery_upload.'</a>'."\n";
		$output.= '</div>'."\n";
		
		return $output;
	}
}

  if (!file_exists($HtPasswdFile)) {
    $fp = @fopen($HtPasswdFile, "w");
    if ($fp) {
      fwrite($fp, "$GuestUsername::read\n");
      fclose($fp);
    }
  }


