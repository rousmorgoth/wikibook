<?php if (!defined('PmWiki')) exit();

$RecipeInfo['syntaxlove']['Version']='2011-10-29';

if (!isset($syntax_style)) {
 $syntax_style = "shThemeDefault.css";
}

$HTMLHeaderFmt[] =
  "<script type='text/javascript' src='\$PubDirUrl/syntaxlove/scripts/shCore.js'></script>";
  
$Array_Syntaxlove =
           array("Bash"   => "0", "CSharp" => "0", "Css"     => "1", "Delphi" => "0",
                 "Groovy" => "0", "Java"   => "1", "JScript" => "1", "Perl"   => "0",
                 "Php"    => "1", "Plain"  => "1", "Python" => "0",  "Ruby"   => "1",
                 "Scala"  => "0", "Sql"    => "1", "Vb"     => "0",  "Xml"    => "1");

foreach ($Array_Syntaxlove as $key => $value){
  switch ($value){
    case "1": $HTMLHeaderFmt[] = "<script type='text/javascript' src='\$PubDirUrl/syntaxlove/scripts/shBrush$key.js'></script>";
  }
}

$HTMLHeaderFmt[] =
" <link type='text/css' rel='stylesheet' href='\$PubDirUrl/syntaxlove/css/shCore.css'/>
  <link type='text/css' rel='stylesheet' href='\$PubDirUrl/syntaxlove/css/$syntax_style'/>
  <script type='text/javascript'>
    SyntaxHighlighter.all();
  </script>";

# this functions activates the options
function syntaxlove_options($options) {
    $class = '';
    $params = array('html'     => 'html-script',      'lightmode' => 'light',      'wrap'       => 'wrap-lines',
                    'gutter'   => 'gutter',           'ruler'     => 'ruler',      'toolbar'    => 'toolbar',
                    'collapse' => 'collapse',         'autolinks' => 'auto-links', 'smarttabs'  => 'smart-tabs',
                    'pad'      => 'pad-line-numbers', 'title'     => 'title',      'firstline'  => 'first-line',
                    'highlight'=> 'highlight',        'tabsize'   => 'tab-size');

    #look to see if the code option has been specified, if not, resort to 'plain' formatting
    $brush = 'plain';
    if(preg_match("/([^ ](\w*)[^= ])/esi",$options,$matches)) {
        $brush = $matches[0];
        foreach($params as $k => $v) {
            if(preg_match("/$k/i", $matches[0], $match)){
                $brush = 'plain';
            }
        }
    }
    $class .= "brush:'".$brush."';";

    #Find options and format them for html
    foreach ($params as $key => $value) {
        if (preg_match("/($key='.*?')/esi", $options,$matches)) {
            $class .= str_replace("$key=", "$value:", $matches[0]).";";
        }
    }
    return ($class);
}


Markup('code',
    '<fulltext',
    '/\\(:codestart(.*?):\\)(.*?)\\(:codeend:\\)/esi',
    "'<:block>'.Keep(str_replace(array(),
    array(), PSS('<pre class=\"'.syntaxlove_options('$1').'\">'.'$2'.'</pre>')))");
