<?php if (!defined('PmWiki')) exit();
/*  Copyright 2004-2006 Patrick R. Michaud (pmichaud@pobox.com)
    Copyright 2006 Hagan Fox (haganfoxATusersDOTsourceforge.net)
    This file is part of PmWiki; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.  See pmwiki.php for full details.

    This recipe script was written as a proof-of-concept exercise to
    demonstrate a proposed feature for the core.

    The script adds a (:thumb <image_filename>:) capability, which will
    produce a thumbnail link to a full-sized view of an uploaded image
    that, in turn, links back to the page.

    Version: 20060315
*/

## Allow override of default thumbnail height in local configuration.
SDV($ThumbLinkHeight, '100');

## (:thumb:) directive
Markup('thumb', 'directives',
  '/\\(:thumb(\\s+.*?)?:\\)/ei',
  "FmtImageLink('', '', array('o' => PSS('$1 ')))");

## FmtImageLink gets the options from (:imagelink:) and generates a
## self-referring link with ?action=imagelink&i=<the_image_name> .
function FmtImageLink($foo, $bar, $opt) {
  global $ImgExtPattern, $ThumbLinkHeight;
  $opt = ParseArgs($opt['o']);
  ## Accept only one "plain" argument.  It must have an image's extension.
  if ($opt['']['1']) { return; } else { $img = $opt['']['0']; }
  if (! preg_match("/($ImgExtPattern)$/", $img)) { return; }
  ## Optionally accept width= and/or height= attributes.
  if ($opt['width'] && $opt['height']) $spacer=' ';
  if (!($opt['width']) && !($opt['height'])) $opt['height']=$ThumbLinkHeight;
  if ($opt['width']) $width='width='.$opt['width'];
  if ($opt['height']) $height = 'height='.$opt['height'];
  $out = '%'.@$width.@$spacer.@$height.'%[[{$FullName}?action=imagelink&amp;i='.$img.'|Attach:'.$img.'"Zv�t�it"]]%%';
  // TODO: I don't know what this does! :-)  ???
  $out = preg_replace('/^(<[^>]+>)(.*)/esm',"PSS('$1').Keep(PSS('$2'))", $out);
  return PRR($out);
}

## Code after this is only for an image-display page.
if ($action != 'imagelink') { return; }

## Create a {$RequestedImage} page variable for ?i=<the_image_to_display>.
if (preg_match("/($ImgExtPattern)$/", @$_REQUEST['i'])) {
  $FmtPV['$RequestedImage'] =
    "'".htmlspecialchars(@$_REQUEST['i'], ENT_QUOTES)."'";
} else { return; }

SDV($HandleActions['imagelink'], 'HandleImageLink');
SDV($HandleAuth['imagelink'], 'read');

## HandleImageLink performs ?action=imagelink, which uses $SiteGroup.ImageLink
## as a template to display the requested image.
function HandleImageLink($pagename, $level = 'read') {
  global $PageImageLink, $FmtV, $HandleImageLinkFmt, $PageStartFmt,
    $PageEndFmt, $HTMLStylesFmt;
  $HTMLStylesFmt['wikicmds'] = "\n $[ #wikicmds { display:none; } ]";
  SDV($HandleImageLinkFmt,array(&$PageStartFmt, '$PageText', &$PageEndFmt));
  SDV($PageImageLink, '$[$SiteGroup/ImageLink]');
  PCache($pagename, RetrieveAuthPage($pagename, 'read'));
  $form = ReadPage(FmtPageName($PageImageLink, $pagename), READPAGE_CURRENT);
  $text = @$form['text'];
  if (!$text) $text = '(:noleft:)(:noright:)(:notitle:)(:noheader:)(:nofooter:)(:nofootmenu:)(:notopmenu:)'
    .'%center margin:0.5em%[[{$FullName}|Attach:{$FullName}/{$RequestedImage}"Zp�t"]]%%';
  $FmtV['$PageText'] = MarkupToHTML($pagename,$text);
  PrintFmt($pagename, $HandleImageLinkFmt);
}
